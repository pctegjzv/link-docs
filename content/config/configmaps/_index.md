+++
title = "配置字典"
Description = "使用 ConfigMap，使配置文件与镜像内容分离，保持容器化应用程序的可移植性。"
weight = 1
+++

ConfigMap 用键值对保存配置数据，可以保存单个属性，也可以保存配置文件。使用 ConfigMap，实现对容器中应用的配置管理，使配置文件与镜像内容分离，保持容器化应用程序的可移植性。

支持环境变量引用完整的 ConfigMap 文件，或引用 ConfigMap 文件中的部分 key；同时支持挂载文件或目录引用完整的 ConfigMap 文件，或引用 ConfigMap 文件中的部分 key 作为文件。

Pod 只能使用同一个命名空间内的 ConfigMaps，不支持跨命名空间去调用。

{{%children style="card" description="true" %}}

