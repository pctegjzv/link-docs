+++
title = "角色绑定"
description = "角色绑定是将角色和指定的一个或多个用户绑定，使这些用户拥有角色的全部权限。"
weight = 2
+++

角色绑定是将角色和指定的一个或多个用户绑定，使这些用户拥有角色的全部权限。

角色绑定支持三种对象：ServiceAccount (服务账号)、User (用户)、Group (组)。角色绑定的类型也是按照命名空间或集群来区分的。

{{%children style="card" description="true" %}}
