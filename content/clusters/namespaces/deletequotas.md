+++
title = "删除资源配额"
Description = "删除已创建的资源配额（ResourceQuota）或资源限制（LimitRange）。"
weight = 8
+++

删除已创建的资源配额（ResourceQuota）或资源限制（LimitRange）。

**操作步骤**

1. 登录 Kubernetes 平台，单击 **集群** > **命名空间**。

2. 单击要删除资源配额或资源限制的 ***命名空间名称***，进入命名空间详情页面。
	* 删除资源配额，在资源配额区段的选择框中，选择待删除的资源配额，并单击 **操作** > **删除**。
	* 删除资源限制，在资源限制区段的选择框中，选择待删除的资源限制，并单击 **操作** > **删除**。
3. 在新窗口中，单击 **确定**。