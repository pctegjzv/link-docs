+++
title = "节点"
Description = ""
weight = 1
+++

Node 是 Kubernetes 集群的工作节点，是 pod 真正运行的主机。Node 可以是物理机，也可以是虚拟机。每个 node 上具有运行 pod 所必需的 container runtime（例如 docker、rkt）、kubelet、kube-proxy 服务。

{{%children style="card" description="true" %}}

