+++
title = "集群"
description = "集群支持添加 node 和创建 namespace 。"
weight = 6
+++

* 集群支持添加 node 作为 pod 运行的主机，node 可以是物理服务器或虚拟机。  

* 支持创建 namespace，创建多个 namespace 相当于在集群中创建多个“虚拟集群”，每个 namespace 可以配置集群内资源配额，并与其它 namespace 的资源隔离，实现集群资源的分配。

本模块包括以下主要功能：

* [节点]({{< relref "clusters/nodes/_index.md" >}})

* [命名空间]({{< relref "clusters/namespaces/_index.md" >}})
